package org.lafzi.lafzi.filters;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.lafzi.android.R;
import org.lafzi.lafzi.adapters.AyatAdapter;
import org.lafzi.lafzi.helpers.database.DbHelper;
import org.lafzi.lafzi.helpers.database.dao.AyatQuranDao;
import org.lafzi.lafzi.helpers.database.dao.AyatQuranDaoFactory;
import org.lafzi.lafzi.helpers.database.dao.IndexDao;
import org.lafzi.lafzi.helpers.database.dao.IndexDaoFactory;
import org.lafzi.lafzi.helpers.preferences.Preferences;
import org.lafzi.lafzi.models.AyatQuran;
import org.lafzi.lafzi.models.FoundDocument;
import org.lafzi.lafzi.utils.HighlightUtil;
import org.lafzi.lafzi.utils.QueryUtil;
import org.lafzi.lafzi.utils.SearchUtil;
import org.lafzi.lafzi.utils.TrigramUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import info.debatty.java.stringsimilarity.Levenshtein;

/**
 * Created by alfat on 21/04/17.
 */

public class AyatAdapterFilter extends Filter {

    private final AyatQuranDao ayatQuranDao;
    private final IndexDao indexDao;

    private final Activity activity;
    private final AyatAdapter adapter;
    private static final int GRAM_COUNT = 3;
    public static final String TAG = "TracingnewUpdate";
    private int maxScore;
    public AyatAdapterFilter(final Activity activity, final AyatAdapter adapter){
        final DbHelper dbHelper = DbHelper.getInstance();
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        ayatQuranDao = AyatQuranDaoFactory.createAyatDao(db);
        indexDao = IndexDaoFactory.createIndexDao(db);

        this.adapter = adapter;
        this.activity = activity;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

        Map<Integer, FoundDocument> matchedDocs = null;
        double threshold = 0.8;
        final boolean isVocal = Preferences.getInstance().isVocal();

        final String queryFinal = QueryUtil.normalizeQuery(constraint.toString(), isVocal);
        maxScore = queryFinal.length() - 2;

        //do {
            try {
                matchedDocs = SearchUtil.search(
                        queryFinal,
                        isVocal,
                        true,
                        true,
                        threshold,
                        indexDao);
            } catch (Exception e) {
                //String suggestion = getSuggestion(queryFinal,alltrigrams);
                //Log.d("TracingnewUpdate", "performFiltering: "+suggestion);
                Log.e("error", "Index JSON cannot be parsed", e);
            }
//            threshold -= 0.1;
//        } while ((matchedDocs.size() < 1) && (threshold >= 0.7));


        List<FoundDocument> matchedDocsValue;
        List<AyatQuran> ayatQurans = new ArrayList<>();

        if (matchedDocs.size() > 0){

            HighlightUtil.highlightPositions(matchedDocs, isVocal, ayatQuranDao);
            matchedDocsValue = getMatchedDocsValues(matchedDocs);

            Collections.sort(matchedDocsValue, new Comparator<FoundDocument>() {
                @Override
                public int compare(FoundDocument o1, FoundDocument o2) {
                    if (o1.getScore() == o2.getScore()){
                        return o1.getAyatQuranId() - o2.getAyatQuranId();
                    }

                    return o1.getScore() < o2.getScore() ? 1 : -1;
                }
            });

            ayatQurans = getMatchedAyats(matchedDocsValue);
//        }else{
//            Map<Integer, FoundDocument> unfilteredDocs;
//            unfilteredDocs = SearchUtil.getUnfilteredDocs();
//
//            if (unfilteredDocs.size() != 0) {
//                Map<Integer, FoundDocument> temp = new HashMap<>();
//                double currentMaxTrigramCounts = -1;
//                for (Map.Entry<Integer, FoundDocument> data : unfilteredDocs.entrySet()) {
//                    double tempValue = data.getValue().getScore();
//                    if (tempValue >= currentMaxTrigramCounts) {
//                        if (tempValue == currentMaxTrigramCounts) {
//                            currentMaxTrigramCounts = data.getValue().getScore();
//                            temp.put(data.getKey(), data.getValue());
//                        } else {
//                            temp.clear();
//                            currentMaxTrigramCounts = data.getValue().getScore();
//                            temp.put(data.getKey(), data.getValue());
//                        }
//                    }
//                }
//                Log.d(TAG, "currentDominantTrigram: " + temp.size() + " " + temp);
//                getSuggestion(temp, queryFinal, indexDao);
//            }
        }

        final FilterResults results = new FilterResults();
        results.values = ayatQurans;
        results.count = ayatQurans.size();

        return results;
    }

    private List<FoundDocument> getMatchedDocsValues(final Map<Integer, FoundDocument> matchedDocs){
        final List<FoundDocument> values = new LinkedList<>();
        for (Map.Entry<Integer, FoundDocument> entry : matchedDocs.entrySet()){
            values.add(entry.getValue());
        }

        return values;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.clear();

        ProgressBar pb = (ProgressBar) activity.findViewById(R.id.searching_progress_bar);
        pb.setVisibility(View.GONE);

        SearchView searchView = (SearchView) activity.findViewById(R.id.search);
        searchView.clearFocus();

        final TextView resultCounter = (TextView) activity.findViewById(R.id.result_counter);
        if (results.count > 0) {
            adapter.addAll((List<AyatQuran>)results.values);

            resultCounter.setText(activity.getString(R.string.search_result_count, results.count));
            resultCounter.setVisibility(View.VISIBLE);
        } else {
            resultCounter.setVisibility(View.GONE);
            TextView tvEmptyResult = (TextView) activity.findViewById(R.id.empty_result);
            tvEmptyResult.setVisibility(View.VISIBLE);
        }

        LinearLayout tvSearchHelp = (LinearLayout) activity.findViewById(R.id.search_help);
        tvSearchHelp.setVisibility(View.GONE);
    }

    private List<AyatQuran> getMatchedAyats(final List<FoundDocument> foundDocuments){

        final List<AyatQuran> ayatQurans = new LinkedList<>();
        for (FoundDocument document : foundDocuments){
            final double relevance = Math.min(Math.floor(document.getScore() / maxScore * 100), 100);

            final AyatQuran ayatQuran = document.getAyatQuran();
            ayatQuran.relevance = relevance;
            ayatQuran.highlightPositions = document.getHighlightPosition();

            ayatQurans.add(ayatQuran);
        }

        return ayatQurans;
    }

    public static String getSuggestion(Map<Integer, FoundDocument> lineTerbanyakList, String query, IndexDao indexDao) {

        final List<String> ngrams = TrigramUtil.nGramsSorted(GRAM_COUNT, query);
        Log.d(TAG, "getSuggestion: " + ngrams.toString());

        Map<Double, String> suggestionList = new HashMap<>();
        Levenshtein calc = new Levenshtein();

        for (Map.Entry<Integer, FoundDocument> lineTerbanyak : lineTerbanyakList.entrySet()) {
            StringBuilder suggestion = new StringBuilder();
            int index = 0;
            double avgSuggestionValue = 0.0;
            int quranId = lineTerbanyak.getValue().getAyatQuranId();
            JSONArray array = indexDao.getNgramById(quranId);
            List<Integer> urutan = lineTerbanyak.getValue().getLis();
            int max = urutan.get(urutan.size() - 1)+2;
            int min = urutan.get(0) - 2;
            if(min<0){
                min =0;
            }
            if(max>array.length()){
                int temp = urutan.get(urutan.size()-1)-max;
                temp = temp*(-1);
                max-=temp;
            }

            if((max-min)>ngrams.size()-1){
                int temp = max-min;
                int decrease = temp-ngrams.size()-1;
                max = max-decrease;
            }
            Log.d(TAG, array.length()+"getSuggestion: max "+max);
            for (int i = min; i < max; i++) {
                try {
                    String currTerm = array.getString(i);
                    if (i == min) {
                        suggestion.append(currTerm);
                    } else {
                        suggestion.append(currTerm.charAt(currTerm.length() - 1));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            double value = calc.distance(query, suggestion.toString());
            suggestion.deleteCharAt(suggestion.length()-1);
            suggestionList.put(value, suggestion.toString());

        }


        String finalSuggestion = "";
        double finalAvg = 999;
        for (Map.Entry<Double, String> data : suggestionList.entrySet()) {
            if (data.getKey() < finalAvg) {
                finalSuggestion = data.getValue();
                finalAvg = data.getKey();
            }
        }
        Log.d(TAG, "suggestionAkhir: " + finalSuggestion.replace("X", "\'") + " avg " + finalAvg);
        return finalSuggestion;
    }

}
