package org.lafzi.lafzi.utils;

import java.util.ArrayList;
import java.util.List;

public class EvaluationModul {
    private static double getRecall(double sumAll, double sumSuggestion) {
        return sumSuggestion / sumAll;
    }

    public static double calculateRecall(List<String> awakenedGolden, List<String> awakenedSystem) {
        return getRecall(awakenedGolden.size(), getSumSystem(awakenedGolden, awakenedSystem));
    }

    private static double getSumSystem(List<String> awakenedGolden, List<String> awakenedSystem) {
        List<String> awakenedTrue = new ArrayList<>();
        for (int i = 0; i < awakenedGolden.size(); i++) {
            for (int j = 0; j < awakenedSystem.size(); j++) {
                if (awakenedGolden.get(i).equals(awakenedSystem.get(j))) {
                    awakenedTrue.add(awakenedSystem.get(j));
                }
            }
        }
        return awakenedTrue.size();
    }

    private static double calculatePrecission(int tp, int sumTpFp) {
        return (double) tp / sumTpFp;
    }

    private static double calculateAp(List<Double> precissions) {
        double sum = 0;
        for (int i = 0; i < precissions.size(); i++) {
            sum += precissions.get(i);
        }

        return sum / precissions.size();
    }

    public static double getAP(List<String> awakenedGolden, List<String> awakenedSystem) {
        double result = 0;
        List<Double> precissions = new ArrayList<>();
        int countSama = 0;
        for (int i = 0; i < awakenedGolden.size(); i++) {
            if (i < awakenedSystem.size()) {
                if (awakenedGolden.get(i).equals(awakenedSystem.get(i))) {
                    countSama++;
                    precissions.add(calculatePrecission(countSama, i + 1));
                }
            }
        }
        if (precissions.size() != 0)
            result = calculateAp(precissions);
        return result;
    }
}
