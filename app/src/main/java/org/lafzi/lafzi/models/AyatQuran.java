package org.lafzi.lafzi.models;

import android.provider.BaseColumns;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alfat on 19/04/17.
 */

public class AyatQuran implements BaseColumns, Serializable {

    public static final String TABLE_NAME = "ayat_quran";
    public static final String SURAH_NO = "surah_no";
    public static final String SURAH_NAME = "surah_name";
    public static final String AYAT_NO = "ayat_no";
    public static final String AYAT_ARABIC = "ayat_arabic";
    public static final String AYAT_INDONESIAN = "ayat_indonesian";
    public static final String AYAT_MUQATHAAT = "ayat_muqathaat";
    public static final String VOCAL_MAPPING_POSITION = "vocal_mapping_position";
    public static final String NONVOCAL_MAPPING_POSITIONG = "nonvocal_mapping_position";

    private  int id;
    private  int surahNo;
    private  String surahName;
    private  int ayatNo;
    private  String ayatArabic;
    private  String ayatIndonesian;
    private  String ayatMuqathaat;
    private  List<Integer> mappingPositions;
    private  String suggestion;
    public List<HighlightPosition> highlightPositions = new ArrayList<>();
    public double relevance;

    public AyatQuran(String suggestion) {
        this.suggestion = suggestion;
    }

    public AyatQuran(int id,
                     int surahNo,
                     String surahName,
                     int ayatNo,
                     String ayatArabic,
                     String ayatIndonesian,
                     String ayatMuqathaat,
                     List<Integer> mappingPosition) {
        this.id = id;
        this.surahNo = surahNo;
        this.surahName = surahName;
        this.ayatNo = ayatNo;
        this.ayatArabic = ayatArabic;
        this.ayatIndonesian = ayatIndonesian;
        this.ayatMuqathaat = ayatMuqathaat;
        this.mappingPositions = mappingPosition;
        this.suggestion = "";
    }

    public String getSuggestion() {
        return suggestion;
    }

    public int getId() {
        return id;
    }

    public int getSurahNo() {
        return surahNo;
    }

    public String getSurahName() {
        return surahName;
    }

    public int getAyatNo() {
        return ayatNo;
    }

    public String getAyatArabic() {
        return ayatArabic;
    }

    public String getAyatIndonesian() {
        return ayatIndonesian;
    }

    public String getAyatMuqathaat() {
        return ayatMuqathaat;
    }

    public List<Integer> getMappingPositions() {
        return mappingPositions;
    }

    @Override
    public String toString() {
        return "AyatQuran{" +
                "id=" + id +
                ", surahNo=" + surahNo +
                ", surahName='" + surahName + '\'' +
                ", ayatNo=" + ayatNo +
                ", ayatArabic='" + ayatArabic + '\'' +
                ", ayatIndonesian='" + ayatIndonesian + '\'' +
                ", ayatMuqathaat='" + ayatMuqathaat + '\'' +
                ", mappingPositions=" + mappingPositions +
                ", suggestion='" + suggestion + '\'' +
                ", highlightPositions=" + highlightPositions +
                ", relevance=" + relevance +
                '}';
    }
}
