package org.lafzi.lafzi.testapps;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.lafzi.lafzi.helpers.database.DbHelper;
import org.lafzi.lafzi.helpers.database.dao.AyatQuranDao;
import org.lafzi.lafzi.helpers.database.dao.AyatQuranDaoFactory;
import org.lafzi.lafzi.helpers.database.dao.IndexDao;
import org.lafzi.lafzi.helpers.database.dao.IndexDaoFactory;
import org.lafzi.lafzi.helpers.preferences.Preferences;
import org.lafzi.lafzi.models.AyatQuran;
import org.lafzi.lafzi.models.FoundDocument;
import org.lafzi.lafzi.utils.HighlightUtil;
import org.lafzi.lafzi.utils.QueryUtil;
import org.lafzi.lafzi.utils.SearchUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.lafzi.lafzi.filters.AyatAdapterFilter.getSuggestion;

public class TestApps {
    private int maxScore;
    public String testSuggestion(String query){
        final AyatQuranDao ayatQuranDao;
        final IndexDao indexDao;
        final DbHelper dbHelper = DbHelper.getInstance();
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        ayatQuranDao = AyatQuranDaoFactory.createAyatDao(db);
        indexDao = IndexDaoFactory.createIndexDao(db);
        Map<Integer, FoundDocument> matchedDocs = null;
        double threshold = 0.8;
        final boolean isVocal = Preferences.getInstance().isVocal();
        final String queryFinal = QueryUtil.normalizeQuery(query, isVocal);
        maxScore = queryFinal.length() - 2;

        //do {
        try {
            matchedDocs = SearchUtil.search(
                    queryFinal,
                    isVocal,
                    true,
                    true,
                    threshold,
                    indexDao);
        } catch (Exception e) {
            //String suggestion = getSuggestion(queryFinal,alltrigrams);
            //Log.d("TracingnewUpdate", "performFiltering: "+suggestion);
            Log.e("error", "Index JSON cannot be parsed", e);
        }

        Map<Integer, FoundDocument> unfilteredDocs = SearchUtil.getUnfilteredDocs();
        if (unfilteredDocs.size() != 0) {
            Map<Integer, FoundDocument> temp = new HashMap<>();
            double currentMaxTrigramCounts = -1;
            for (Map.Entry<Integer, FoundDocument> data : unfilteredDocs.entrySet()) {
                double tempValue = data.getValue().getScore();
                if (tempValue >= currentMaxTrigramCounts) {
                    if (tempValue == currentMaxTrigramCounts) {
                        currentMaxTrigramCounts = data.getValue().getScore();
                        temp.put(data.getKey(), data.getValue());
                    } else {
                        temp.clear();
                        currentMaxTrigramCounts = data.getValue().getScore();
                        temp.put(data.getKey(), data.getValue());
                    }
                }
            }
             return getSuggestion(temp, queryFinal, indexDao);
        }else{
            return "";
        }
    }
    public List<AyatQuran> test(String query){
        final AyatQuranDao ayatQuranDao;
        final IndexDao indexDao;
        final DbHelper dbHelper = DbHelper.getInstance();
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        ayatQuranDao = AyatQuranDaoFactory.createAyatDao(db);
        indexDao = IndexDaoFactory.createIndexDao(db);
        Map<Integer, FoundDocument> matchedDocs = null;
        double threshold = 0.8;
        final boolean isVocal = Preferences.getInstance().isVocal();
        final String queryFinal = QueryUtil.normalizeQuery(query, isVocal);
        maxScore = queryFinal.length() - 2;

        //do {
        try {
            matchedDocs = SearchUtil.search(
                    queryFinal,
                    isVocal,
                    true,
                    true,
                    threshold,
                    indexDao);
        } catch (Exception e) {
            //String suggestion = getSuggestion(queryFinal,alltrigrams);
            //Log.d("TracingnewUpdate", "performFiltering: "+suggestion);
            Log.e("error", "Index JSON cannot be parsed", e);
        }
//            threshold -= 0.1;
//        } while ((matchedDocs.size() < 1) && (threshold >= 0.7));


        List<FoundDocument> matchedDocsValue;
        List<AyatQuran> ayatQurans = new ArrayList<>();

        if (matchedDocs.size() > 0){

            HighlightUtil.highlightPositions(matchedDocs, isVocal, ayatQuranDao);
            matchedDocsValue = getMatchedDocsValues(matchedDocs);

            Collections.sort(matchedDocsValue, new Comparator<FoundDocument>() {
                @Override
                public int compare(FoundDocument o1, FoundDocument o2) {
                    if (o1.getScore() == o2.getScore()){
                        return o1.getAyatQuranId() - o2.getAyatQuranId();
                    }

                    return o1.getScore() < o2.getScore() ? 1 : -1;
                }
            });

            ayatQurans = getMatchedAyats(matchedDocsValue);
        }

        return ayatQurans;
    }

    private List<FoundDocument> getMatchedDocsValues(final Map<Integer, FoundDocument> matchedDocs){
        final List<FoundDocument> values = new LinkedList<>();
        for (Map.Entry<Integer, FoundDocument> entry : matchedDocs.entrySet()){
            values.add(entry.getValue());
        }

        return values;
    }

    private List<AyatQuran> getMatchedAyats(final List<FoundDocument> foundDocuments){

        final List<AyatQuran> ayatQurans = new LinkedList<>();
        for (FoundDocument document : foundDocuments){
            final double relevance = Math.min(Math.floor(document.getScore() / maxScore * 100), 100);

            final AyatQuran ayatQuran = document.getAyatQuran();
            ayatQuran.relevance = relevance;
            ayatQuran.highlightPositions = document.getHighlightPosition();

            ayatQurans.add(ayatQuran);
        }

        return ayatQurans;
    }
}
