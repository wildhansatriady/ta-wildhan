package org.lafzi.lafzi.testapps;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.lafzi.android.R;
import org.lafzi.lafzi.models.AyatQuran;
import org.lafzi.lafzi.utils.EvaluationModul;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import info.debatty.java.stringsimilarity.Levenshtein;

public class TestActivity extends AppCompatActivity {
    List<ModelTestest> ayatBangkitBenar;
    List<ModelTestest> ayatBangkitTypo;
    List<ModelTestest> ayatBangkitSuggestion;
    List<String> listSuggestion;
    List<Double> evalTypo;
    List<Double> evalSuggest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl");
        evalTypo = new ArrayList<>();
        evalSuggest = new ArrayList<>();
        ayatBangkitBenar = new ArrayList<>();
        ayatBangkitTypo = new ArrayList<>();
        ayatBangkitSuggestion = new ArrayList<>();
        listSuggestion = new ArrayList<>();
        TestApps test = new TestApps();


//        List<AyatQuran> data = test.test("SAYASLANARANZATALAHAB");
//
//        for (int i = 0; i < data.size(); i++) {
//            Log.d("iniSatuan ", "onCreate: "+data.get(i).getSurahNo()+":"+data.get(i).getAyatNo());
//        }


        generalTest(test);


    }

    void generalTest(TestApps test) {
        StringBuilder ayat = new StringBuilder();
        TextView tv = findViewById(R.id.textView);
        for (int i = 0; i < queryBenar.length; i++) {
            List<AyatQuran> data = test.test(queryBenar[i]);
            List<String> tempAyatBangkit = new ArrayList<>();
            for (int j = 0; j < data.size(); j++) {
                String ayatBangkit = data.get(j).getSurahNo() + ":" + data.get(j).getAyatNo();
                tempAyatBangkit.add(ayatBangkit);
            }
            ayatBangkitBenar.add(new ModelTestest(i, tempAyatBangkit));
        }

        for (int i = 0; i < querySalah.length; i++) {
            List<AyatQuran> data = test.test(querySalah[i]);
            List<String> tempAyatBangkit = new ArrayList<>();
            for (int j = 0; j < data.size(); j++) {
                String ayatBangkit = data.get(j).getSurahNo() + ":" + data.get(j).getAyatNo();
                tempAyatBangkit.add(ayatBangkit);
            }
            ayatBangkitTypo.add(new ModelTestest(i, tempAyatBangkit));
            listSuggestion.add(test.testSuggestion(querySalah[i]).replace("X", "\'"));
        }

        for (int i = 0; i < ayatBangkitBenar.size(); i++) {
            ayat.append("Index ").append(i + 1);
            List<String> golden = ayatBangkitBenar.get(i).getArray();
            List<String> system = ayatBangkitTypo.get(i).getArray();
            double result = EvaluationModul.calculateRecall(golden, system);
            evalTypo.add(result);
            ayat.append(" : ").append(result).append("\n");
        }


        tv.setText(ayat);
        Log.d("hiyahiya", "onCreate: " + listSuggestion.toString());
        for (int i = 0; i < listSuggestion.size(); i++) {
            List<String> tempAyatBangkit = new ArrayList<>();
            if (!listSuggestion.get(i).equals("")) {
                List<AyatQuran> data = test.test(listSuggestion.get(i));
                for (int j = 0; j < data.size(); j++) {
                    String ayatBangkit = data.get(j).getSurahNo() + ":" + data.get(j).getAyatNo();
                    tempAyatBangkit.add(ayatBangkit);
                }
            }
            ayatBangkitSuggestion.add(new ModelTestest(i, tempAyatBangkit));
        }

        for (int i = 0; i < ayatBangkitBenar.size(); i++) {
            ayat.append("Index ").append(i + 1);
            List<String> golden = ayatBangkitBenar.get(i).getArray();
            List<String> system = ayatBangkitSuggestion.get(i).getArray();
            double result = EvaluationModul.calculateRecall(golden, system);
            evalSuggest.add(result);
            ayat.append(" : ").append(result).append("\n");
        }

        int countZerotypo = 0;
        int countNonZerotypo = 0;
        int countZeroSuggest = 0;
        int countNonZeroSuggest = 0;
        double avgSystem = 0.0;
        double avgSuggest = 0.0;
        for (int i = 0; i < evalTypo.size(); i++) {
            avgSystem += evalTypo.get(i);
            avgSuggest += evalSuggest.get(i);
            if (evalTypo.get(i) == 0.0) {
                countZerotypo++;
            } else {
                countNonZerotypo++;
            }

            if (evalSuggest.get(i) == 0.0) {
                countZeroSuggest++;
            } else {
                countNonZeroSuggest++;
            }
        }


        avgSuggest = avgSuggest / 50;
        avgSystem = avgSystem / 50;
        tv.setText(ayat +
                "\n Zero Typo" + countZerotypo + "\n Non Zero Typo " + countNonZerotypo +
                "\n ZeroSuggest " + countZeroSuggest + "\n Non Zero Suggest " + countNonZeroSuggest + "" +
                "\n CountSalah " + querySalah.length + "\n Count Benar " + queryBenar.length +
                "\n avgSystem " + avgSystem + "\n avgSuggest " + avgSuggest);

        String[] columns = {"No", "Text Al-Quran", "Terdapat(Gold Standard)", "Query Benar", "Query(User)", "Edit Distance", "Terdapat di(User)", "Suggestion", "Terdapat di(Suggestion)", "Recall Benar", "Recall Typo", "Recall Suggestion", "AP Benar", "AP Typo", "AP Suggestion"};

        Workbook workbook = new XSSFWorkbook();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Evaltuation Result");


        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
        }

        // Create Other rows and cells with employees data
        int rowNum = 1;
        for (int j = 0; j < ayatBangkitBenar.size(); j++) {
            ModelTestest goldStandard = ayatBangkitBenar.get(j);
            ModelTestest userStandard = ayatBangkitTypo.get(j);
            ModelTestest suggestionStandard = ayatBangkitSuggestion.get(j);
            String typoWord = querySalah[j];
            String trueWord = queryBenar[j];
            double distance = new Levenshtein().distance(trueWord, typoWord);
            String suggestionWord = listSuggestion.get(j);
            double resultTypo = EvaluationModul.calculateRecall(goldStandard.getArray(), userStandard.getArray());
            double resultBenar = EvaluationModul.calculateRecall(goldStandard.getArray(), goldStandard.getArray());
            double resultSuggestion = EvaluationModul.calculateRecall(goldStandard.getArray(), suggestionStandard.getArray());
            double resultAPTypo = EvaluationModul.getAP(goldStandard.getArray(), userStandard.getArray());
            double resultApSuggestion = EvaluationModul.getAP(goldStandard.getArray(), suggestionStandard.getArray());
            double resultApBenar = EvaluationModul.getAP(goldStandard.getArray(), goldStandard.getArray());
            int index = j + 1;
            Row row = sheet.createRow(rowNum++);

            //index
            row.createCell(0)
                    .setCellValue(index);

            //goldStandard
            row.createCell(2)
                    .setCellValue(goldStandard.getArray().toString());

            //goldWord
            row.createCell(3)
                    .setCellValue(trueWord);

            //typoWord
            row.createCell(4)
                    .setCellValue(typoWord);

            //editDistance
            row.createCell(5)
                    .setCellValue(distance);

            //terdapatDi(User)
            row.createCell(6)
                    .setCellValue(userStandard.getArray().toString());

            //suggestion
            row.createCell(7)
                    .setCellValue(suggestionWord);

            //terdapat(suggestion)
            row.createCell(8)
                    .setCellValue(suggestionStandard.getArray().toString());

            //recall benar
            row.createCell(9)
                    .setCellValue(resultApBenar);

            //recall typo
            row.createCell(10)
                    .setCellValue(resultTypo);

            //recall Suggestion
            row.createCell(11)
                    .setCellValue(resultSuggestion);
            //AP Benar
            row.createCell(12)
                    .setCellValue(resultApBenar);
            //AP typo
            row.createCell(13)
                    .setCellValue(resultAPTypo);
            //AP suggestion
            row.createCell(14)
                    .setCellValue(resultApSuggestion);

        }


        // Write the output to a file
        FileOutputStream fileOut = null;
        try {
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File file = new File(path, "Data_pengaduan.xlsx");
            try {
                BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                workbook.write(output);
                output.close();
                Toast.makeText(getBaseContext(), "Berhasil " + path.getPath(), Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Closing the workbook
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static class ModelTestest {
        int index;
        List<String> array;

        @Override
        public String toString() {
            return "ModelTestest{" +
                    "index=" + index +
                    ", array=" + array +
                    '}';
        }

        public ModelTestest(int index, List<String> array) {
            this.index = index;
            this.array = array;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public List<String> getArray() {
            return array;
        }

        public void setArray(List<String> array) {
            this.array = array;
        }
    }

    String queryBenar[] = new String[]{
            "FIKULUBIHIM",
            "YAKADULBARKUYAHTA",
            "KUL'A'UZUBIRABINAS",
            "MINSARIMAHALAK",
            "FIZIDIHAHABLUMIMASAD",
            "SAYASLANARANZATALAHAB",
            "FASABIHBIHAMDIRABIKAWASTAGFIR",
            "FASALILIRABIKAWANHAR",
            "FAWAYLULILMUSALIN",
            "BALNAHNUMAHRUMUN",
            "WALAYASTASNUN",
            "FASATUBSIRUWAYUBSIRUN",
            "FADUWALILAH",
            "KABURAMAKTAN'INDALAHI'AN",
            "SABAHALILAHIMAFISAMAWATI",
            "FIKITABIMAKNUN",
            "LAYAMASUHU'ILALMUTAHARUN",
            "FASARIBUNASURBALHIM",
            "FISIDRIMAHDUD",
            "WATALHIMANDUD",
            "WASABIKUNASABIKUN",
            "TABARAKASMURABI",
            "WAFISAMUDA'IZKILALAHUMTAMAT",
            "KUTILALHARASUN",
            "FALZARIYATIYUSRA",
            "WAZARIYATIZARWA",
            "ALAHUGAFURARAHIMA",
            "WAYLULIKULI'AFAKIN'ASIM",
            "ALAYKUMHALMINHALIKINGAYRULAH",
            "LAMAKTU'ATIWALAMAMNU'AH",
            "KULUMAN'ALAYHAFAN",
            "ALATATGAWFILMIZAN",
            "FIHAFAKIHATUW",
            "KAZABATSAMUDUBINUZUR",
            "WAKURABIGFIRWARHAMWA'ANTAHAYRURAHIMIN",
            "KALAHSA'UFIHAWALATUKALIMUN",
            "MUSTAKBIRINABIHISAMIRAN",
            "KALARABINSURNIBIMAKAZABUN",
            "WALAHUMAKAM",
            "BALNAKZIFUBILHAKI",
            "LATARAFIHA",
            "FAYAZARUHAKA",
            "ZANATU'ADNINTAZRIMIN",
            "HARUNA'AHI",
            "YAFKAHUKAWLI",
            "LAYAMLIKUNASAF",
            "ZANATI'ADNILA",
            "BISMILAHIRAHMAN",
            "RAHMANIRAHIM",
            "ALHAMDULILAH"};

    String[] querySalah = new String[]{
            "FIJULUBIHIN",
            "YAKADULVARKUYAHRA",
            "KUL'A'UZUVIRAVINAS",
            "NINSARINAHALAK",
            "DIZIDIHAHABLUNINASAD",
            "SAYASLAMARANZATALAHAV",
            "DASABIHBIHANDIRABIKAWASTAGDIR",
            "FASALILIRAVIKAWAMHAR",
            "FAWATLULILMUSALIM",
            "VALMAHMUMAHRUMUM",
            "WALAYASTASMUM",
            "FASATUVSIRUWAYUVSIRUM",
            "FADUEALILAH",
            "KAVURAMAKTAM'IMDALAHI'AM",
            "SAVAHALILAHIMAFISAMAWATI",
            "FUKUTABUMAKNUN",
            "KAYAMASUHU'UKAKMUTAHARUN",
            "FASARUBUNASURBAKHUM",
            "FUSUDRUMAHDUD",
            "WATAKHUMANDUD",
            "WASABUKUNASABUKUN",
            "RAVARAKADMUTABI",
            "WAFUSAMUDA'UZKUKAKAHUMTAMAT",
            "KUTUKAKHARASUN",
            "FAKZARUYATUYUSRA",
            "WAZARUYATUZARWA",
            "AKAHUGAFURARAHUMA",
            "EAYLULIKULI'AFAKIM'ASIM",
            "ALAYKUMHALMIMHALIKIMGAYRULAH",
            "LAMAKTU'ATIEALAMAMMU'AH",
            "KULUMAM'AKAYHAFAN",
            "ALATATGAEFILMIZAM",
            "FIHAFAKIHATUE",
            "KAZABATSAMUDUBIMUZUR",
            "EAKURABIGFIREARHAMEA'AMTAHAYRURAHIMIM",
            "KALAHSA'UFIHAEALARUKALIMUM",
            "MUDTAKBIRIMABIHISAMIRAM",
            "KALARABIMSURMIBIMAKAZABUM",
            "WALAJUMAKAM",
            "VALNAKZIFUVILJAKI",
            "LATARAFIJA",
            "FAYAZARUJAKA",
            "ZANATU'AFNINRAZRIMIM",
            "JARUNA'AJI",
            "YAFKAJUKAWLI",
            "LAYAMKIKUMASAF",
            "XANATI'ADMILA",
            "VISMILAJIRAJMAN",
            "RAJMAMIRAHIM",
            "ALJAMDULILAH"

    };
}
