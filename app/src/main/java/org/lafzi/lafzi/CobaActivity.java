package org.lafzi.lafzi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.lafzi.android.R;

import info.debatty.java.stringsimilarity.CharacterSubstitutionInterface;
import info.debatty.java.stringsimilarity.JaroWinkler;
import info.debatty.java.stringsimilarity.MetricLCS;
import info.debatty.java.stringsimilarity.NGram;
import info.debatty.java.stringsimilarity.NormalizedLevenshtein;
import info.debatty.java.stringsimilarity.QGram;
import info.debatty.java.stringsimilarity.WeightedLevenshtein;

public class CobaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coba);




        findViewById(R.id.bt_calculate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaroWinkler calc = new JaroWinkler();
                TextView tvHasil = findViewById(R.id.hasil);
                EditText etParam1 = findViewById(R.id.editText);
                EditText etParam2 = findViewById(R.id.editText2);
                double distance = calc.distance(etParam1.getText().toString(),etParam2.getText().toString());
                double similarity = calc.similarity(etParam1.getText().toString(),etParam2.getText().toString());
                tvHasil.setText("distance "+distance+" similarity "+similarity);
            }
        });
    }
}
