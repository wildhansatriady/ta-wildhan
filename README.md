# Lafzi Android
Sourcode ini adalah sourcecode yang sudah dimodifikasi dan ditambahkan suggestion system pada branch windowed_v2
Sourcecode asli dapat dilihat di : 
https://github.com/lafzi/lafzi-android

## Lisensi
GPL (GNU General Public License) v3. Anda bebas menggunakan, memodifikasi, dan mendistribusikan software ini dengan syarat tetap menjadikannya open-source, dan mencantumkan link ke source code versi asli
